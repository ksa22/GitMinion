# Base stage for shared environment setup
FROM node:18-slim AS base
WORKDIR /app

# Dependencies stage to install dependencies
FROM base AS deps
COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile --network-timeout 100000

# Builder stage to build the application
FROM base AS builder
COPY --from=deps /app/node_modules ./node_modules
COPY . .
RUN yarn build 

# Production stage with Nginx
FROM nginx:stable-alpine AS production
# Copy built assets from builder stage
COPY --from=builder /app/build /usr/share/nginx/html
# Set up configuration
COPY nginx.conf /etc/nginx/nginx.conf
EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]

# -------------------------------------------------------------------------- #

# # Use an official Node runtime as a parent image
# FROM node:18-slim AS base
# WORKDIR /app

# # Install dependencies only when needed
# FROM base AS deps
# COPY package.json yarn.lock ./
# RUN yarn install --frozen-lockfile --network-timeout 100000

# # Rebuild the source code only when needed
# FROM base AS builder
# COPY --from=deps /app/node_modules ./node_modules
# COPY . .

# # Production image, copy all the files and run the application
# FROM node:18-slim AS production
# WORKDIR /app
# COPY --from=builder /app ./
# EXPOSE 3000
# CMD ["yarn", "start"]
