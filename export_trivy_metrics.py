from prometheus_client import CollectorRegistry, Gauge, push_to_gateway
import json
import os

def parse_trivy_report(report_path):
    with open(report_path, 'r') as file:
        data = json.load(file)
        results = data.get('Results', [])
        vulnerabilities = []
        misconfigurations = []
        secrets = []
        for result in results:
            if 'Vulnerabilities' in result:
                vulnerabilities.extend(result['Vulnerabilities'])
            if 'Misconfigurations' in result:
                misconfigurations.extend(result['Misconfigurations'])
            if 'Secrets' in result:
                secrets.extend(result['Secrets'])
        return vulnerabilities, misconfigurations, secrets

def push_metrics_to_gateway(vulnerabilities, misconfigurations, secrets, job_name, gateway_url):
    registry = CollectorRegistry()
    severity_gauges = {
        'CRITICAL': Gauge('trivy_critical_vulnerabilities', 'Number of critical vulnerabilities', registry=registry),
        'HIGH': Gauge('trivy_high_vulnerabilities', 'Number of high vulnerabilities', registry=registry),
        'MEDIUM': Gauge('trivy_medium_vulnerabilities', 'Number of medium vulnerabilities', registry=registry),
        'LOW': Gauge('trivy_low_vulnerabilities', 'Number of low vulnerabilities', registry=registry),
        'UNKNOWN': Gauge('trivy_unknown_vulnerabilities', 'Number of unknown vulnerabilities', registry=registry),
        'MISCONFIGURATIONS': Gauge('trivy_misconfigurations', 'Number of misconfigurations', registry=registry),
        'SECRETS': Gauge('trivy_secrets', 'Number of exposed secrets', registry=registry),
    }

    severity_counts = {key: 0 for key in severity_gauges.keys()}
    for vuln in vulnerabilities:
        severity = vuln.get('Severity')
        severity_counts[severity] += 1
    severity_counts['MISCONFIGURATIONS'] = len(misconfigurations)
    severity_counts['SECRETS'] = len(secrets)
    
    for severity, count in severity_counts.items():
        severity_gauges[severity].set(count)
    
    push_to_gateway(gateway_url, job=job_name, registry=registry)

if __name__ == "__main__":
    report_path = 'trivy-report.json'
    gateway_url = os.getenv('PUSHGATEWAY_URL', 'http://localhost:9091')
    job_name = os.getenv('TRIVY_JOB_NAME', 'trivy_scan')
    vulnerabilities, misconfigurations, secrets = parse_trivy_report(report_path)
    push_metrics_to_gateway(vulnerabilities, misconfigurations, secrets, job_name, gateway_url)