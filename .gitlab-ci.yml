workflow:
  rules:
    - if: $CI_COMMIT_REF_NAME == "main"
      variables:
        MS_NAME: "${CI_PROJECT_NAME}-prod"
        USERNAME: ${PROD_USERNAME}
        HOST: ${PROD_HOST}
        DEPLOY_KEY: ${PROD_DEPLOY_KEY}
    - if: $CI_COMMIT_REF_NAME == "dev"
      variables:
        MS_NAME: "${CI_PROJECT_NAME}-dev"
        USERNAME: ${DEV_USERNAME}
        HOST: ${DEV_HOST}
        DEPLOY_KEY: ${DEV_DEPLOY_KEY}
    - when: always      

before_script:
- export MS_NAME=$(echo "$MS_NAME" | tr '[:upper:]' '[:lower:]')
- export TEST_IMAGE_NAME="$CI_REGISTRY_IMAGE/$MS_NAME/test:$CI_COMMIT_SHORT_SHA"
- export REL_IMAGE_NAME="$CI_REGISTRY_IMAGE/$MS_NAME/release:$CI_COMMIT_SHORT_SHA"
- export LATEST_IMAGE_NAME="$CI_REGISTRY_IMAGE/$MS_NAME/cache:latest"

variables:
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
  GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  DOCKER_HOST: tcp://docker:2375
  PUSHGATEWAY_URL: 'http://${HOST}:9091'
  TRIVY_JOB_NAME: 'trivy_scan'

# add dynamic-analysis

stages:
  # - test
  - build
  - my_dast
  - sast
  - release
  - deploy

build:
  stage: build
  image: docker:latest
  services:
    - docker:dind
  script:
    - docker pull $LATEST_IMAGE_NAME || true  # Ensures the build doesn't fail if the image doesn't exist yet
    - docker build --cache-from $LATEST_IMAGE_NAME -t $TEST_IMAGE_NAME -t $LATEST_IMAGE_NAME .
    - echo $CI_JOB_TOKEN | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    - docker push $TEST_IMAGE_NAME
    - docker push $LATEST_IMAGE_NAME
  only:
    - main
    - dev

my_dast:
  stage: my_dast
  image: docker:latest
  services:
    - docker:dind
  variables:
    IMAGE_FULL_NAME: "$REL_IMAGE_NAME"
  script:
    # Download and install Go
    - wget https://go.dev/dl/go1.23.1.linux-amd64.tar.gz
    - rm -rf /usr/local/go && tar -C /usr/local -xzf go1.23.1.linux-amd64.tar.gz
    - export PATH=$PATH:/usr/local/go/bin
    
    # Install Nuclei
    - go install -v github.com/projectdiscovery/nuclei/v2/cmd/nuclei@latest
    
    # Add Go binaries and Nuclei to PATH
    - export PATH=$PATH:/root/go/bin  # Or wherever Go installs binaries (you might need to adjust this based on your setup)
    
    # Check Nuclei version to verify installation
    - nuclei -version
    - echo $IMAGE_FULL_NAME
    # Replace the image placeholder in the docker-compose file
    - sed -i "s|PLACEHOLDER_IMAGE|$IMAGE_FULL_NAME|g" docker-compose.yml
    
    # Pull Docker images and run containers
    - docker-compose pull
    - docker-compose up -d
    
    # List Docker containers
    - docker ps -a
    
    # Wait for the service to start
    - sleep 5
    
    # Run Nuclei against the local service
    - nuclei -u http://localhost:3000



sonarqube-check:
  stage: sast
  image: 
    name: sonarsource/sonar-scanner-cli:5.0
    entrypoint: [""]
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script: 
    - sonar-scanner -Dsonar.host.url=$SONAR_HOST_URL -X -Dsonar.qualitygate.wait=true
    # - sonar-scanner -X -Dsonar.qualitygate.wait=true
  allow_failure: true
  only:
    - main
    - dev

trivy_scan:
  stage: sast
  # stage: trivy
  before_script:
    # - apt-get update && apt-get upgrade -y
    # - apt-get install -y curl python3 python3-pip python3-venv
    # - python3 -m venv prometheus-env
    # - source prometheus-env/bin/activate
    # - pip3 install prometheus_client
    - curl -sfL https://raw.githubusercontent.com/aquasecurity/trivy/main/contrib/install.sh | sh -s -- -b /usr/local/bin v0.50.2
  script:
    - trivy --scanners vuln,misconfig,secret image osamayusuf/gitminion:dev
    # - trivy --format json --output trivy-report.json --scanners vuln,misconfig,secret image osamayusuf/gitminion:dev
    # - trivy --format json --output trivy-report.json --scanners vuln,misconfig,secret image $TEST_IMAGE_NAME
    # - source prometheus-env/bin/activate
    # - python3 export_trivy_metrics.py
  only:
    - main
    - dev
  # artifacts:
  #   expire_in: 1 day
  #   paths:
  #     - trivy-report.json
  #   reports:
  #     container_scanning: trivy-report.json

unit-test:
  stage: sast
  image: node:latest
  script:
    - npm install
    # - npm run test  # Assuming 'test' is configured in your package.json
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - node_modules/
  only:
    - main
    - dev

lint:
  stage: sast
  image: node:latest
  script:
    - npm install
    # - npm run lint  # Assuming 'lint' is a script in your package.json
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - node_modules/
  only:
    - main
    - dev

release_image:
  stage: release
  image: docker:latest
  services:
    - docker:dind
  script:
    - echo $CI_JOB_TOKEN | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY
    - echo $TEST_IMAGE_NAME
    - docker pull $TEST_IMAGE_NAME
    - docker tag $TEST_IMAGE_NAME $REL_IMAGE_NAME
    - docker push $REL_IMAGE_NAME
  only:
    - main
    - dev

deploy:
  stage: deploy
  image: alpine:latest
  variables:
    IMAGE_FULL_NAME: "$REL_IMAGE_NAME"
  before_script:
    - echo "image name:${REL_IMAGE_NAME}"
    - echo "image full name:${IMAGE_FULL_NAME}"
    - apk add --no-cache openssh-client git
    - 'which ssh-agent || ( apk add openssh-client )'
    - eval $(ssh-agent -s)
    - echo "$DEPLOY_KEY" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - ssh-keyscan $HOST >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
  script:
    - |
      GIT_STRATEGY=none ssh $USERNAME@$HOST <<EOF
        cd GitMinion
        rm -f bak.yml
        git checkout -- docker-compose.yml

        # Setting up Git credentials for pulling the latest changes 
        git config --global credential.helper 'store --file .git/credentials'
        echo "https://oauth2:$GITLAB_PATO@gitlab.com" > .git/credentials
        git pull
        rm .git/credentials

        # Logging into GitLab's Docker Registry
        echo $CI_JOB_TOKEN | docker login -u $CI_REGISTRY_USER --password-stdin $CI_REGISTRY

        # Updating the docker-compose.yml with the new image name
        sed -i "s|image: 'PLACEHOLDER_IMAGE'|image: '${IMAGE_FULL_NAME}'|" docker-compose.yml

        # Pulling the updated Docker image and starting the service
        docker-compose pull
        docker-compose up -d

        cp docker-compose.yml bak.yml

        # Revert changes to the docker-compose.yml to its default state
        git checkout -- docker-compose.yml
      EOF
  only:
    - main
    - dev